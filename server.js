const express = require('express');

const app = express();

const PORT = 5001;

app.use(express.json());

// This line requires the routes defined in 'routes.js' and 
// passes the Express app (app variable) as a parameter to the exported function
require('./app/routes')(app, {});

app.listen(PORT, () => {
    console.log('Running on port ' + PORT);
})