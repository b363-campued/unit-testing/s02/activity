function factorial(n) {
    if(n===0) return 1;
    if(n===1) return 1;
    return n * factorial(n-1);
    // 5 * 4 * 3 * 2 * 1 = 120

}

function div_check(n) {
    if(n % 5 === 0) return true;
    if(n % 7 === 0) return true;
    return false;
}


//Mock database
const names = {
    "Brandon": {
        "name": "Brandon Boyd",
        "alias": "Brad",
        "age": 35
    }, 
    "Steve": {
        "name": "Steve Tyler",
        "alias": "Steevy",
        "age": 56
    },
}

module.exports = {
    factorial: factorial,
    div_check: div_check,
    names: names
}