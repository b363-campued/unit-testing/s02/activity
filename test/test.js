//import factorial function from utils module
const { factorial, div_check } = require('../src/util.js');

//import expect and assert methods from chain module
const {expect, assert} = require('chai');


// Test Suites are made up of collection of test cases that should be executed together

// The describe() function is commonly used in JavaScript testing frameworks like Mocha to group related test cases together and create a test suite.
// describe('Test Suite Description', function() {
  // Test cases go here
//});

//'Test Suite Description': This is a string that describes what the test suite is testing or what aspect of your code it focuses on. 
//It serves as a human-readable label for the group of test cases.

//function() { ... }: This is a callback function that contains one or more it() blocks, which represent individual test cases. 
//Inside this function, you structure and organize your test cases that are related to the described feature or component.

describe('test_fun_factorials', ()=> {
    
    it('test_fun_factorial_5!_is_120', ()=> {
        const product = factorial(5);
        expect(product).to.equal(120);
    })
    
    //The it() function defines a test case with the description 'test_fun_factorial_5!_is_120', which describes what the test is checking.
    
    //Inside the test case function, const product = factorial(5); calculates the factorial of 5 by calling the factorial() function.
    
    //expect(product).to.equal(120); is a Chai assertion that checks whether the value of product (the result of factorial(5)) is equal to 120. 
    // If it's equal, the test case will pass; otherwise, it will fail.
    
    // To run: CLI > npm test

    it('test_fun_factorial_1!_is_1', ()=> {
        const product = factorial(1);
        assert.equal(product,1);
    })
    
    it('test_fun_factorial_0!_is_1', ()=> {
        const product = factorial(0);
        assert.equal(product,1);
    })

    it('test_fun_factorial_4!_is_24', ()=> {
        const product = factorial(4);
        assert.equal(product,24);
    })

    it('test_fun_factorial_10!_is_3628800', ()=> {
        const product = factorial(10);
        assert.equal(product,3628800);
    })

    
})


//it() function is used to define individual test cases or specifications within a test suite. 
//it(description, function)

//description: This is a string that describes what the test case is supposed to check or verify. 
//It serves as a human-readable label for the test case, helping you and others understand the purpose of the test.

//function: This is a callback function that contains the actual test code. Inside this function, 
//you write one or more assertions using Chai's assertion methods (e.g., expect, assert, or should) to verify that the code being tested behaves as expected.
//The test passes if all assertions within this function evaluate to true, and it fails if any assertion evaluates to false

// You can use either expect or assert but each of them have its own methods available
// 'expect' returning expected and actual value
// 'assert' - checking if function is return correct result

//https://www.chaijs.com/api/assert/
//https://www.chaijs.com/api/bdd/


/*
S2 Activity Instructions:

1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
a. If the number received is divisible by 5, return true.
b. If the number received is divisible by 7, return true.
c. Return false if otherwise
3. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
4. Initialize your local git repository, add the remote link and push to git with the commit message of “Add activity code S2".
5. Add the link in Boodle.

*/

describe('test_divisibility_by_5_or_7', ()=> {


    it('test_100_is_divisible_by_5', ()=> {
        const isDivisible = div_check(100);
        assert.isTrue(isDivisible);
    })
    
    it('test_49_is_divisible_by_7', ()=> {
        const isDivisible = div_check(49);
        assert.isTrue(isDivisible);
    })

    it('test_30_is_divisible_by_5', ()=> {
        const isDivisible = div_check(30);
        assert.isTrue(isDivisible);
    })
    it('test_56_is_divisible_by_7', ()=> {
        const isDivisible = div_check(56);
        assert.isTrue(isDivisible);
    })


})




