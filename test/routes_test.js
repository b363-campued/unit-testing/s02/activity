//testing routes.js code
const chai = require('chai');

const expect  = chai.expect;
//same as const {expect} = require("chai")

const http = require('chai-http');
chai.use(http);

describe('api_test_suite', () => {

    //test if route /peope get return a value
    it('test_api_people_is_running', ()=> {
        chai.request('http://localhost:5001')
        //specify the type of HTTP request
        .get('/people')
        //method that handles error and response that will received from the endpoint
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
        })
    })

    it('test_api_people_returns_200', (done)=> {
        chai.request('http://localhost:5001')
        //specify the type of HTTP request
        .get('/people')
        //method that handles error and response that will received from the endpoint
        .end((err, res) => {
            expect(res.status).to.equal(200);
            //best practice used in asynchronous test cases as it signal end of test case
            /*
                Using done() is a best practice in asynchronous testing with libraries like Mocha when you are 
                dealing with asynchronous operations or assertions. Here's why it's important:

                Handling Asynchronous Code: Many tests involve asynchronous operations like making API requests, reading files, or 
                waiting for timers to complete. done() is used to signal to Mocha that a test is asynchronous and should wait for 
                the asynchronous code to finish before considering the test complete.

                Ensuring Proper Completion: Without done(), Mocha might not wait for asynchronous operations to complete 
                before moving on to the next test or finishing the test suite. This can lead to unreliable test results and 
                false positives or negatives.

                Timeout Handling: Mocha has a default timeout for tests. If your test doesn't signal completion with done(), 
                and the asynchronous operation takes longer than the timeout, Mocha will consider the test as failed, even 
                if the operation would eventually succeed.

            */
            done();
        })
    })


    //test response status is 400 if !req.body.hasOwnProperty('name')
    it('test_api_post_person_returns_400_if_no_person_name', (done)=> {
        chai.request('http://localhost:5001')
        .post('/person')
        //specifies type of input to be sent out as part of the POST request
        .type('json')
        .send({
            alias: "Jason",
            age: 28
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })


    it('test_api_post_person_is_running', (done)=> {
        chai.request('http://localhost:5001')
        .post('/person')
        .type('json')
        .send({
            name: "Jason",
            alias: "Jas",
            age: 28
        })
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
            done();
        })
    })

    // it('test_api_post_person_returns_400_if_name_is_not_string', (done)=> {
    //     chai.request('http://localhost:5001')
    //     .post('/person')
    //     //specifies type of input to be sent out as part of the POST request
    //     .type('json')
    //     .send({
    //         name: 123,
    //         age: 28
    //     })
    //     .end((err, res) => {
    //         expect(res.status).to.equal(400);
    //         done();
    //     })
    // })

    it('test_api_post_person_returns_400_if_no_ALIAS', (done)=> {
        chai.request('http://localhost:5001')
        .post('/person')
        //specifies type of input to be sent out as part of the POST request
        .type('json')
        .send({
            name: "Franz",
            age: 26
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_person_returns_400_if_no_AGE', (done)=> {
        chai.request('http://localhost:5001')
        .post('/person')
        //specifies type of input to be sent out as part of the POST request
        .type('json')
        .send({
            name: "Franz",
            alias: "Renzo",
            address: "Baguio City"
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

})