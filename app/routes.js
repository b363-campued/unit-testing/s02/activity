const { names } = require('../src/util.js');

//This exports a function that takes an app object as a parameter. 
//This function is meant to define routes on the provided Express app.

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
        }
        if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }

        return res.send({
            person: req.body
        });
    })
}
